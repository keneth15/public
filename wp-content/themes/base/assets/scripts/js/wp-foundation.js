/*
These functions make sure WordPress
and Foundation play nice together.
*/

jQuery(document).ready(function() {
	var $win = jQuery(window),
		$body = jQuery('body');

	// Remove empty P tags created by WP inside of Accordion and Orbit
	jQuery('.accordion p:empty, .orbit p:empty').remove();

	// Adds Flex Video to YouTube and Vimeo Embeds
	jQuery('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
		if ( jQuery(this).innerWidth() / jQuery(this).innerHeight() > 1.5 ) {
		  jQuery(this).wrap("<div class='widescreen responsive-embed'/>");
		} else {
		  jQuery(this).wrap("<div class='responsive-embed'/>");
		}
	});
	jQuery(window).on('load', function() {
		jQuery('#ae-cform-input-reg-email-1').attr('placeholder', 'Email');

		jQuery('.top-bar .menu-bars').on('click', function() {
			jQuery(this).find('.fas').toggleClass('fa-times');
		});
		jQuery('.js-off-canvas-overlay').on('click', function() {
			jQuery('.top-bar .menu-bars .fas').removeClass('fa-times');
		});

		jQuery('.js-subscribe a').on('click', function(e) {
			e.preventDefault();

			var $target = jQuery(this).attr('href');

			jQuery('html, body').animate({
				scrollTop: jQuery($target).offset().top
			}, 800);

			if ( jQuery(this).parents('.off-canvas') ) {
				jQuery('.js-off-canvas-overlay').trigger('click');
			}
		});
		

		if( $body.hasClass('home') ) {
			var $banner = jQuery('.js-banner'),
				$videos = jQuery('.js-video-list'),
				$releases = jQuery('.js-release-list'),
				$menu = jQuery('#main-nav li a');

			// $menu.on('click', function(e) {
			// 	var $title = jQuery(this).attr('title').toLowerCase();

			// 	if($title) {
			// 		e.preventDefault();

			// 		jQuery('.menu-primary li').removeClass('active');
			// 		jQuery(this).parent().addClass('active');

			// 		jQuery('html, body').animate({
			// 			scrollTop: jQuery('.section-'+$title).offset().top - 64
			// 		}, 800);
			// 	}
			// });
			
			$banner.slick({
				dots: false,
				fade: true,
				cssEase: 'linear',
				responsive: [
				  {
					breakpoint: 815,
					settings: {
						arrows: false
					}
				  }
				]
			});

			jQuery('.section-banner').addClass('init');

            $banner.on('beforeChange', function(event, slick, currentSlide, nextSlide){
              $banner.addClass('is-sliding');
            });
      
            $banner.on('afterChange', function(event, slick, currentSlide){
              $banner.removeClass('is-sliding');
            });

			$releases.slick({
				asNavFor: '.album-cover-list'
			});

			jQuery('.album-cover-list').slick({
				arrows: false,
				dots: false,
				fade: true,
				cssEase: 'linear',
				asNavFor: '.js-release-list',
				focusOnSelect: true
			});

            $releases.on('afterChange', function(event, slick, currentSlide){
              $releases.addClass('slided');
            });

			$videos.slick({
				dots: false,
				fade: true,
				cssEase: 'linear',
			});
      
            $videos.on('afterChange', function(event, slick, currentSlide){
              $videos.addClass('slided');
            });

			jQuery('.article-video .button').on('click', function(e) {
				e.preventDefault();
		
				var youtubeID = jQuery(this).data('id'),
					embedCode = "<div class='media-youtube-video'><iframe src='https://www.youtube.com/embed/" + youtubeID + "?showinfo=0&autohide=1&rel=0&autoplay=1&wmode=transparent' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe></div>",
					$parent = jQuery(this).parents('.type-video');
					
				if (!document.querySelector('.media-youtube-video')) {
					$parent.addClass('playing');
					$parent.append(embedCode);
					jQuery('.js-video-list').addClass('has-embed');
				} else {
					$parent.removeClass('playing');
					jQuery('.media-youtube-video').remove();
					jQuery('.js-video-list').removeClass('has-embed');
				}
			});
			jQuery('.js-video-list > button').on('click', function(){
				if (jQuery('.article-video').hasClass('playing')) {
					jQuery('.media-youtube-video').remove();
					jQuery('.article-video').removeClass('playing');
					jQuery('.js-video-list').removeClass('has-embed');
				}
			});

			jQuery('.js-intro-arrow').on('click', function(e){
				e.preventDefault();

				jQuery('html, body').animate({
					scrollTop: jQuery('.section-about').offset().top - 64
				}, 500);
			});

			jQuery('.main-home .section').each(function(){
				var self = jQuery(this);
			
				jQuery(this).waypoint({
					handler: function(direction) {
						// if(direction == 'down') {
						// }

						// jQuery('.main-home .section').removeClass('show');
						self.addClass('show');
					}, offset: '50%'
				});
			});

			if( $win.width() < 480) {

				var waypoints = jQuery('.section-about').waypoint({
					handler: function(direction) {
					  if(direction == 'up') {
						jQuery('#top-bar-menu .logo').removeClass('show');
					  } else {
						jQuery('#top-bar-menu .logo').addClass('show');
					  }
					}, offset: '0'
				  });
			}
		}
	});
}); 