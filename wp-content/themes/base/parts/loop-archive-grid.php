<?php
/**
 * The template part for displaying a grid of posts
 *
 * For more info: http://jointswp.com/docs/grid-archive/
 */

	// Adjust the amount of rows in the grid
	$grid_columns = 3; 

	if( has_post_thumbnail() ) {
		$thumb = get_the_post_thumbnail_url(get_the_ID(), 'large');
	} else {
		$thumb = $GLOBALS['dir'].'/images/blank-news.png';
	}
?>

<?php if( 0 === ( $wp_query->current_post  )  % $grid_columns ): ?>

    <div class="grid-x grid-margin-x grid-padding-x archive-grid" data-equalizer> <!--Begin Grid--> 

<?php endif; ?> 

		<!--Item: -->
		<div class="small-6 medium-3 large-3 cell panel" data-equalizer-watch>
			
			<article id="post-<?php the_ID(); ?>" <?php post_class('article'); ?> role="article">
				<header class="article-header">
					<div class="date"><?php the_time('m.d.y'); ?></div>
					<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				</header>
				<div class="article-thumb" style="background-image: url('<?php echo $thumb; ?>');">
					<div class="article-thumb_overlay hide-for-small-only">
						<a href="<?php the_permalink(); ?>" title="Read article" class="button secondary">Read more</a>
					</div>
					<img src="<?php echo $GLOBALS['dir']; ?>/images/blank-news_sq.gif" alt="">
				</div>
			</article>
			
		</div>

<?php if( 0 === ( $wp_query->current_post + 1 )  % $grid_columns ||  ( $wp_query->current_post + 1 ) ===  $wp_query->post_count ): ?>

   </div>  <!--End Grid --> 

<?php endif; ?>

