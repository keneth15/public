<?php
	/**
	 * Template part for displaying home content
	 */
	$releases = new WP_Query( array('post_type' => 'release', 'posts_per_page' => 10, 'orderby' => 'date', 'order' => 'DESC' ) );
	$news = new WP_Query( array('post_type' => 'post', 'posts_per_page' => 3, 'orderby' => 'date', 'order' => 'DESC' ) );
	$videos = new WP_Query( array('post_type' => 'video', 'posts_per_page' => 10, 'orderby' => 'date', 'order' => 'DESC' ) );
	$products = new WP_Query( array('post_type' => 'product', 'posts_per_page' => 4, 'orderby' => 'date', 'order' => 'DESC' ) );
	$about = new WP_Query( array( 'pagename' => 'about') );

	$moreMerch = get_field('shop_link', 'option');
	$bitArtist = get_field('bit_artist', 'option');

	if( $moreMerch ) {
		$mm_url = $moreMerch['url'];
		$mm_title = $moreMerch['title'];
		$mm_target = $moreMerch['target'] ? $moreMerch['target'] : '_self';

		if( $mm_title ) {
			$moreMerch_title = $moreMerch['title'];
		} else {
			$moreMerch_title = 'Shop all';
		}
	}
?>

<?php if( have_rows('banners', 'options') ): ?>
<section class="section section-banner">
	<div class="banner-list js-banner">
	<?php 
		$num = 0;
		while( have_rows('banners', 'options') ): the_row(); 
		$banner = get_sub_field('banner');
		$banner_mob = get_sub_field('banner_mobile');
		$content = get_sub_field('editor_tool');
		$cta = get_sub_field('cta');
		$vert = get_sub_field('layout_vertical');
		$hori = get_sub_field('layout_horiztonal');

		if( $cta ) {
			$url = $cta['url'];
			$title = $cta['title'];
			$target = $cta['target'] ? $cta['target'] : '_self';

			if( $title ) {
				$cta_title = $cta['title'];
			} else {
				$cta_title = 'Stream / Download';
			}
		}

		if( $vert == 'vt' ) {
			$layout_v = ' vert-vt';
		} elseif( $vert == 'vc' ) {
			$layout_v = ' vert-vc';
		} elseif( $vert == 'vb' ) {
			$layout_v = ' vert-vb';
		}

		if( $hori == 'hl' ) {
			$layout_h = ' hori-hl';
		} elseif( $hori == 'hc' ) {
			$layout_h = ' hori-hc';
		} elseif( $hori == 'hr' ) {
			$layout_h = ' hori-hr';
		}
	?>
		<div class="banner-item banner-item-<?php echo $num; ?>">
			<div class="banner-item_content">
				<div class="grid-container<?php echo $layout_v; ?><?php echo $layout_h; ?>">
					<div class="content">
						<?php echo $content; ?>
						<?php if( $cta ) : ?>
						<div class="cta">
							<a href="<?php echo $url; ?>" target="<?php echo $target; ?>" title="<?php echo $cta_title; ?>" class="button"><?php echo $cta_title; ?></a>
						</div>
						<?php endif; ?>
					</div>

					<a href="#" class="arrow js-intro-arrow"></a>
				</div>
			</div>
			<figure class="banner-item_figure">
				<div class="banner-item_image hide-for-small-only" style="background-image: url('<?php echo $banner; ?>');">
				</div>
				<div class="banner-item_image show-for-small-only" style="background-image: url('<?php echo $banner_mob; ?>');">
				</div>
			</figure>
			<img src="<?php echo $GLOBALS['dir']; ?>/images/blank-banner.gif" alt="" class="hide-for-small-only">
			<img src="<?php echo $GLOBALS['dir']; ?>/images/blank-banner-mob.gif" alt="" class="show-for-small-only">
		</div>
	<?php $num++; endwhile; ?>
	</div>
</section>
<?php endif; wp_reset_query(); ?>

<?php if( $releases->have_posts() ) : ?>
<section class="section section-releases section-music">
	<div class="grid-container grid-x">
		<div class="cell small-12 medium-10 medium-offset-1">
			
			<header class="section-header">
				<h2>Latest Releases</h2>
			</header>
			<div class="release-list js-release-list">
				<?php 
					$num = 0;
					while ( $releases->have_posts() ) : $releases->the_post(); 
					$link = get_field('steam_download');
					$subtitle = get_field('release_subtitle');
								
					if( $link ) {
						$url = $link['url'];
						$title = $link['title'];
						$target = $link['target'] ? $link['target'] : '_self';

						if( $title ) {
							$link_title = $link['title'];
						} else {
							$link_title = 'Stream / Download';
						}
					}
				?>
				<article id="post-<?php the_ID(); ?>" <?php post_class('article article-'.$num); ?> role="article" itemscope itemtype="http://schema.org/Article">
					<div class="grid-x align-middle">
						<div class="cell small-12 medium-offset-1 large-offset-1 medium-4 large-4 cell-left">
							<div class="album">
								<div class="cover">
									<div class="s"></div>
									<div class="s"></div>
									<div class="s"></div>
									<div class="s"></div>
									<div class="s"></div>
								</div>
								<?php the_post_thumbnail('full'); ?>
							</div>
						</div>
						<div class="cell small-12 medium-offset-1 large-offset-1 medium-5 large-5 cell-right">
							<header class="article-header">
								<?php if ( $subtitle ) : ?>
								<div class="subtitle"><?php echo $subtitle; ?></div>
								<?php endif; ?>
								<div class="surprise">
									<h3 class="word word-rel"><?php the_title(); ?></h3>
								</div>
								<?php if( $link ) : ?>
								<div class="cta">
									<a href="<?php echo $url; ?>" target="<?php echo $target; ?>" title="<?php echo $link_title; ?>" class="button">
										<span><?php echo $link_title; ?></span>
										<div class="marquee" aria-hidden="true">
											<div class="marquee__inner">
												<span><?php echo $link_title; ?></span>
												<span><?php echo $link_title; ?></span>
												<span><?php echo $link_title; ?></span>
												<span><?php echo $link_title; ?></span>
											</div>
										</div>
									</a>
								</div>
								<?php endif; ?>
							</header>
						</div>
					</div>
				</article>
				<?php $num++; endwhile; ?>
			</div>
		</div>
		<footer class="cell small-12 medium-12 section-footer">
			<a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Music' ) ) ); ?>" title="View all releases">View all releases</a>
		</footer>
	</div>

	<div class="album-cover">
		<div class="album-cover-list">
			<?php 
				while ( $releases->have_posts() ) : $releases->the_post(); 
				if( has_post_thumbnail() ) {
					$cover = get_the_post_thumbnail_url(get_the_ID(),'full');
				} else {
					// $cover = $GLOBALS['dir'].'/images/blank-news.png';
					$cover = '';
				}
			?>
			<div class="album-cover-item" style="background-image: url('<?php echo $cover; ?>');"></div>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; wp_reset_query(); ?>

<?php if( $videos->have_posts() ) : ?>
<section class="section section-videos">
	<div class="grid-container grid-x">
		<div class="cell small-12 medium-10 medium-offset-1">
			<header class="section-header">
				<h2>Videos</h2>
			</header>

			<div class="video-list js-video-list">
				<?php 
					$num = 0;
					while ( $videos->have_posts() ) : $videos->the_post(); 
					$yt_id = get_field('youtube_id');
					$title = get_the_title();
					$maxPos = 40;
					if (strlen($title) > $maxPos) {
						$lastPos = ($maxPos - 3) - strlen($title);
						$title = substr($title, 0, strrpos($title, ' ', $lastPos)) . '...';
					}

					if(wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) )) {
						$thumb = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
					} else {
						$thumb = 'https://img.youtube.com/vi/'.$yt_id.'/maxresdefault.jpg';
					}
				?>
				<article id="post-<?php the_ID(); ?>" <?php post_class('article article-video article-'.$num); ?> role="article" itemscope itemtype="http://schema.org/Article">
					<div class="grid-x grid-padding-x">
						<div class="cell small-12 medium-8 large-8 cell-left">
							<figure class="article-figure">
								<div class="article-thumb">
									<div class="article-thumb_overlay">
										<a href="#" title="Play" class="button" data-id="<?php echo $yt_id; ?>">
											<span>Play Video</span>
											<div class="marquee" aria-hidden="true">
												<div class="marquee__inner">
													<span>Play Video</span>
													<span>Play Video</span>
													<span>Play Video</span>
													<span>Play Video</span>
												</div>
											</div>
											
										</a>
										<div class="cover">
											<div class="s"></div>
											<div class="s"></div>
											<div class="s"></div>
											<div class="s"></div>
											<div class="s"></div>
										</div>
									</div>
									<div class="thumb" style="background-image: url('<?php echo $thumb; ?>');"></div>
									<img src="<?php echo $GLOBALS['dir']; ?>/images/blank-video.gif" alt="">
								</div>
							</figure>
						</div>
						<div class="cell small-12 medium-4 large-4 cell-right">
							<header class="article-header">
								<?php if ( $num == 0) : ?>
								<div class="subtitle">Featured Video</div>
								<?php endif; ?>
								<div class="surprise">
									<h3 class="word word-vid"><?php echo $title; ?></h3>
								</div>
							</header>
						</div>
					</div>
				</article>
				<?php $num++; endwhile; ?>
			</div>
		</div>

		<footer class="cell small-12 medium-12 section-footer">
			<a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Videos' ) ) ); ?>" title="View all videos">View all videos</a>
		</footer>
	</div>
</section>
<?php endif; wp_reset_query(); ?>

<?php if( $bitArtist ): ?>
<section class="section section-tour">
	<div class="grid-container grid-x">
		<div class="cell small-12 medium-10 medium-offset-1">
			<header class="section-header">
				<h2>Tour</h2>
			</header>
			<div class="section-content">
				<script charset="utf-8" src="https://widget.bandsintown.com/main.min.js"></script><a class="bit-widget-initializer" data-artist-name="<?php echo $bitArtist; ?>" data-display-local-dates="false" data-display-past-dates="false" data-auto-style="false" data-text-color="#000000" data-link-color="#00b4b3" data-background-color="rgba(0,0,0,0)" data-display-limit="3" data-display-start-time="false" data-link-text-color="#FFFFFF" data-display-lineup="false" data-display-play-my-city="false" data-separator-color="rgba(255, 255, 255, 0.5)"></a>
			</div>
		</div>

		<footer class="cell small-12 medium-12 section-footer">
			<a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Tour' ) ) ); ?>" title="View All Tourdates">View All Tourdates</a>
		</footer>
	</div>
</section>
<?php endif; wp_reset_query(); ?>

<?php if( $products->have_posts() ) : ?>
<section class="section section-merch">
	<div class="grid-container grid-x">
		<div class="cell small-12 medium-10 medium-offset-1">
			<header class="section-header">
				<h2>Store</h2>
			</header>

			<div class="section-content">
				<div class="merch-list grid-x grid-margin-x">
					<?php 
						while ( $products->have_posts() ) : $products->the_post(); 
						
						$price = get_field('price');
						$link = get_field('link');
									
						if( $link ) {
							$url = $link['url'];
							$title = $link['title'];
							$target = $link['target'] ? $link['target'] : '_self';

							if( $title ) {
								$link_title = $link['title'];
							} else {
								$link_title = 'Buy Now';
							}
						}
					?>
					<div class="cell small-12 medium-3">
						<article class="merch-item">
							<figure class="article-figure">
								<div class="article-thumb" style="background-image: url('<?php the_post_thumbnail_url('full'); ?>">

									<?php if( $link ) : ?>
									<div class="article-thumb_overlay">
										<a href="<?php echo $url; ?>" target="<?php echo $target; ?>" title="<?php echo $link_title; ?>" class="button">
											<span><?php echo $link_title; ?></span>
											<div class="marquee" aria-hidden="true">
												<div class="marquee__inner">
													<span><?php echo $link_title; ?></span>
													<span><?php echo $link_title; ?></span>
													<span><?php echo $link_title; ?></span>
													<span><?php echo $link_title; ?></span>
												</div>
											</div>
										</a>
									</div>
									<?php endif; ?>

									<img src="<?php echo $GLOBALS['dir']; ?>/images/blank-sq.gif" alt="">
								</div>
							</figure>
							<header class="article-header">
								<h3><?php the_title(); ?></h3>
								<?php if( $price ) : ?>
								<span class="price"><?php echo $price; ?></span>
								<?php endif; ?>
							</header>
						</article>
					</div>
					<?php endwhile; ?>
				</div><!--.merch-list-->
			</div>
			<?php if( $moreMerch ) : ?>
			<footer class="cell small-12 medium-12 section-footer">
				<a href="<?php echo $mm_url; ?>" target="<?php echo $mm_target; ?>" title="<?php echo $moreMerch_title; ?>"><?php echo $moreMerch_title; ?></a>
			</footer>
			<?php endif; ?>
		</div>
	</div>
	
</section>
<?php endif; wp_reset_query(); ?>