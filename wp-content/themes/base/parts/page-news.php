<?php 
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 
	$news = new WP_Query( array('post_type' => 'post', 'posts_per_page' => 15, 'orderby' => 'date', 'order' => 'DESC', 'paged' => $paged ) ); 
?>

<div class="grid-container">

	<div class="news-list grid-x grid-padding-x">
		<?php 
			while ( $news->have_posts() ) : $news->the_post(); 
				
			$categories = get_the_category( $post->ID );
			$media_source = get_field('external_source');
				
			if( has_post_thumbnail() ) {
				$thumb = get_the_post_thumbnail_url(get_the_ID(),'news-img');
				$noThumb = '';
			} else {
				$thumb = $GLOBALS['dir'].'/images/blank-news.jpg';
				$noThumb = ' no-thumb';
			}
							
			if( $media_source ) {
				$url = $media_source['url'];
				$title = $media_source['title'];
				$target = $media_source['target'] ? $media_source['target'] : '_self';

				if( $title ) {
					$media_title = $media_source['title'];
				} else {
					$media_title = 'Media Source';
				}
				$perma = '<a href="'.$url.'" target="'.$target.'" title="'.get_the_title().'">'.get_the_title().'</a>';
				$read = '<a href="'.$url.'" target="'.$target.'" title="Read article" class="button"><span>View Now</span><div class="marquee" aria-hidden="true"><div class="marquee__inner"><span>View Now</span><span>View Now</span><span>View Now</span><span>View Now</span></div></div></a>';
			} else {
				$perma = '<a href="'.get_the_permalink().'">'.get_the_title().'</a>';
				$read = '<a href="'.get_the_permalink().'" title="Read article" class="button"><span>View Now</span><div class="marquee" aria-hidden="true"><div class="marquee__inner"><span>View Now</span><span>View Now</span><span>View Now</span><span>View Now</span></div></div></a>';
			}
		?>
			<div class="cell medium-4 news-item">
				<article id="post-<?php the_ID(); ?>" <?php post_class('article'); ?> role="article" itemscope itemtype="http://schema.org/Article">
					<div class="article-thumb<?php echo $noThumb; ?>" style="background-image: url(<?php echo $thumb; ?>);">
						<div class="article-thumb_overlay">
						<?php echo $read; ?>
						</div>
						<img src="<?php echo $GLOBALS['dir']; ?>/images/blank-news.gif" alt="">
					</div>
					<header class="article-header">
						<span class="cat">
						<?php if ( !empty($categories) ) : ?>
							<?php foreach ( $categories as $category ) : ?>
								<?php echo $category->name; ?>
							<?php endforeach; ?>
						<?php endif; ?>
						<?php if( $media_source ) : ?> / <a href="<?php echo $url; ?>" target="<?php echo $target; ?>" title="<?php echo $media_title; ?>"><?php echo $media_title; ?></a><?php endif; ?>
						</span>
						<h3><?php echo $perma; ?></h3>
					</header>
				</article>
			</div>
		<?php endwhile; ?>
	</div><!-- .news-list -->
    <?php if (function_exists("pagination")) { pagination($news->max_num_pages); } ?>
</div>