<?php
/**
 * Template part for displaying a single post
 */
	if( has_post_thumbnail() ) {
		$thumb = get_the_post_thumbnail_url(get_the_ID(), 'full');
	} else {
		$thumb = '';
		$noFeatured = 'no-featured';
	}
				
	$categories = get_the_category( $post->ID );
	$media_source = get_field('external_source');
?>

<div class="backpage">
	<a href="<?php echo esc_url( get_permalink( get_page_by_title( 'News' ) ) ); ?>" title="Back to all News"><i class="fas fa-arrow-left"></i> Back to all News</a>
</div>

<article id="post-<?php the_ID(); ?>" <?php post_class($noFeatured); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
	
	<header class="article-header">
		<?php if( has_post_thumbnail() ) : ?>
			<?php the_post_thumbnail('full'); ?>
		<?php endif; ?>

		<span class="cat">
		<?php if ( !empty($categories) ) : ?>
			<?php foreach ( $categories as $category ) : ?>
				<?php echo $category->name; ?>
			<?php endforeach; ?>
		<?php endif; ?>
		<?php if( $media_source ) : ?> / <a href="<?php echo $url; ?>" target="<?php echo $target; ?>" title="<?php echo $media_title; ?>"><?php echo $media_title; ?></a><?php endif; ?>
		</span>
		<h3><?php the_title(); ?></h3>

	</header>
    <div class="article-content" itemprop="text">
	    <?php the_content(); ?>
	</div>
	<footer class="article-footer">
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'jointswp' ), 'after'  => '</div>' ) ); ?>
	</footer>
</article>