<?php 
	$feat = new WP_Query( array('post_type' => 'release', 'posts_per_page' => 1, 'orderby' => 'date', 'order' => 'DESC' ) ); 
	while ($feat->have_posts()) { $feat->the_post(); 
		$featID = get_the_ID();
	}
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 
	$release = new WP_Query( array('post_type' => 'release', 'posts_per_page' => 9, 'orderby' => 'date', 'order' => 'DESC', 'paged' => $paged, 'post__not_in' => array($featID) ) ); 
?>

<div class="grid-container grid-x">
	<div class="cell small-12 medium-10 medium-offset-1">
		<header class="section-header">
			<h2>Featured Release</h2>
		</header>
		<div class="featured">
			<?php 
				while ( $feat->have_posts() ) : $feat->the_post(); 
			
				$link = get_field('steam_download');
				$subtitle = get_field('release_subtitle');
							
				if( $link ) {
					$url = $link['url'];
					$title = $link['title'];
					$target = $link['target'] ? $link['target'] : '_self';
			
					if($title) {
						$link_title = $link['title'];
					} else {
						$link_title = 'Stream / Download';
					}
				}
			?>
			<article id="post-<?php the_ID(); ?>" <?php post_class('article grid-x align-middle'); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
				<div class="cell small-12 medium-5 large-5">
					<div class="article-thumb">
						<?php the_post_thumbnail('full'); ?>
					</div>
				</div>
				<div class="cell small-12 medium-6 large-6 medium-offset-1 large-offset-1">
					<header class="article-header">
						<?php if ( $subtitle ) : ?>
						<div class="subtitle"><?php echo $subtitle; ?></div>
						<?php endif; ?>
						<h3><?php the_title(); ?></h3>
					</header>
					<?php if($link) : ?>
					<div class="cta">
						<a href="<?php echo $url; ?>" target="<?php echo $target; ?>" title="<?php echo $link_title; ?>" class="button">
							<span><?php echo $link_title; ?></span>
							<div class="marquee" aria-hidden="true">
								<div class="marquee__inner">
									<span><?php echo $link_title; ?></span>
									<span><?php echo $link_title; ?></span>
									<span><?php echo $link_title; ?></span>
									<span><?php echo $link_title; ?></span>
								</div>
							</div>
						</a>
					</div>
					<?php endif; ?>
				</div>
			</article>
			<?php endwhile; ?>
		</div>
	</div>
</div>
<?php wp_reset_query(); ?>
<div class="grid-container">
	
	<div class="music-list grid-x grid-padding-x">
		<?php 
			while ( $release->have_posts() ) : $release->the_post(); 
			$subtitle = get_field('release_subtitle');
			$link = get_field('steam_download');
						
			if( $link ) {
				$url = $link['url'];
				$title = $link['title'];
				$target = $link['target'] ? $link['target'] : '_self';
		
				if($title) {
					$link_title = $link['title'];
				} else {
					$link_title = 'Stream / Download';
				}
			}
		?>
			<div class="cell medium-4">
				<article id="post-<?php the_ID(); ?>" <?php post_class('article'); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
					<div class="article-thumb secondary">
						<div class="article-thumb_overlay hide-for-small-only">
							<?php if ( $subtitle ) : ?>
							<div class="subtitle"><?php echo $subtitle; ?></div>
							<?php endif; ?>
							<h3><?php the_title(); ?></h3>
							<?php if($link) : ?>
							<div class="cta">
								<a href="<?php echo $url; ?>" target="<?php echo $target; ?>" title="<?php echo $link_title; ?>" class="button">
									<span><?php echo $link_title; ?></span>
									<div class="marquee" aria-hidden="true">
										<div class="marquee__inner">
											<span><?php echo $link_title; ?></span>
											<span><?php echo $link_title; ?></span>
											<span><?php echo $link_title; ?></span>
											<span><?php echo $link_title; ?></span>
										</div>
									</div>
								</a>
							</div>
							<?php endif; ?>
						</div>

						<?php the_post_thumbnail('full'); ?>
					</div>
					<header class="article-header show-for-small-only">
						<?php if ( $subtitle ) : ?>
						<div class="subtitle"><?php echo $subtitle; ?></div>
						<?php endif; ?>
						<h3><?php the_title(); ?></h3>
						<?php if($link) : ?>
						<div class="cta">
							<a href="<?php echo $url; ?>" target="<?php echo $target; ?>" title="<?php echo $link_title; ?>" class="button">
								<span><?php echo $link_title; ?></span>
								<div class="marquee" aria-hidden="true">
									<div class="marquee__inner">
										<span><?php echo $link_title; ?></span>
										<span><?php echo $link_title; ?></span>
										<span><?php echo $link_title; ?></span>
										<span><?php echo $link_title; ?></span>
									</div>
								</div>
							</a>
						</div>
						<?php endif; ?>
					</header>

				</article>
			</div>
		<?php endwhile; ?>
	</div><!-- .music-list -->
    <?php if (function_exists("pagination")) { pagination($release->max_num_pages); } ?>
</div>