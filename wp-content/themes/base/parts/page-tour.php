<?php 
	$bitArtist = get_field('bit_artist', 'option');
?>

<section class="section grid-container grid-x">
	<div class="cell small-12 medium-10 medium-offset-1">
		<header class="section-header">
			<h2><?php the_title(); ?></h2>
		</header>

		<?php if( $bitArtist ): ?>
		<div class="section-body">
			<script charset="utf-8" src="https://widget.bandsintown.com/main.min.js"></script><a class="bit-widget-initializer" data-artist-name="<?php echo $bitArtist; ?>" data-display-local-dates="false" data-display-past-dates="false" data-auto-style="false" data-text-color="#000000" data-link-color="#00b4b3" data-background-color="rgba(0,0,0,0)" data-display-limit="" data-display-start-time="false" data-link-text-color="#FFFFFF" data-display-lineup="false" data-display-play-my-city="false" data-separator-color="rgba(255, 255, 255, 0.5)"></a>
		</div>
		<?php endif; wp_reset_query(); ?>
	</div>
</section>