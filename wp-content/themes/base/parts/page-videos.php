<?php 

	$feat = new WP_Query( array('post_type' => 'video', 'posts_per_page' => 1, 'orderby' => 'date', 'order' => 'DESC' ) ); 
	while ($feat->have_posts()) { $feat->the_post(); 
		$featID = get_the_ID();
	}
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 
	$videos = new WP_Query( array('post_type' => 'video', 'posts_per_page' => 9, 'orderby' => 'date', 'order' => 'DESC', 'paged' => $paged, 'post__not_in' => array($featID) ) ); 
?>

<div class="grid-container grid-x">
	<div class="cell small-12 medium-10 medium-offset-1">
		<header class="section-header">
			<h2>Videos</h2>
		</header>
		<div class="featured">
			<?php 
				while ( $feat->have_posts() ) : $feat->the_post(); 
				$yt_id = get_field('youtube_id');
			
				if(wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) )) {
					$thumb = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
				} else {
					$thumb = 'https://img.youtube.com/vi/'.$yt_id.'/maxresdefault.jpg';
				}
			?>
			<article id="post-<?php the_ID(); ?>" <?php post_class('article grid-x grid-margin-x align-middle'); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
				<div class="cell small-12 medium-8">
					<div class="article-thumb" style="background-image: url('<?php echo $thumb; ?>');">
						<div class="article-thumb_overlay">
							<a href="https://www.youtube.com/watch?v=<?php echo $yt_id; ?>" title="Watch <?php the_title(); ?>" class="button" data-fancybox>
								<span>Play Video</span>
								<div class="marquee" aria-hidden="true">
									<div class="marquee__inner">
										<span>Play Video</span>
										<span>Play Video</span>
										<span>Play Video</span>
										<span>Play Video</span>
									</div>
								</div>
							</a>
						</div>

						<img src="<?php echo $GLOBALS['dir']; ?>/images/blank-video.gif" alt="">
					</div>
				</div>
				<div class="cell small-12 medium-4">
					<header class="article-header">
						<div class="subtitle hide-for-small-only">Latest Video</div>
						<h3><?php the_title(); ?></h3>
					</header>
				</div>
			</article>
			<?php endwhile; ?>
		</div>
	</div>
</div>
<?php wp_reset_query(); ?>
<div class="grid-container">
	<div class="video-list grid-x grid-padding-x">
		<?php 
			while ( $videos->have_posts() ) : $videos->the_post(); 
			$yt_id = get_field('youtube_id');
		
			if(wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) )) {
				$thumb = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
			} else {
				$thumb = 'https://img.youtube.com/vi/'.$yt_id.'/maxresdefault.jpg';
			}
		?>
			<div class="cell medium-4">
				<article id="post-<?php the_ID(); ?>" <?php post_class('article'); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
					<div class="article-thumb">
						<div class="article-thumb_overlay">
							<a href="https://www.youtube.com/watch?v=<?php echo $yt_id; ?>" title="Watch <?php the_title(); ?>" class="button" data-fancybox>
								<span>Play Video</span>
								<div class="marquee" aria-hidden="true">
									<div class="marquee__inner">
										<span>Play Video</span>
										<span>Play Video</span>
										<span>Play Video</span>
										<span>Play Video</span>
									</div>
								</div>
							</a>

							<h3 class="hide-for-small-only"><?php the_title(); ?></h3>
						</div>
				
						<div class="article-thumb" style="background-image: url('<?php echo $thumb; ?>');">
							<img src="<?php echo $GLOBALS['dir']; ?>/images/blank-video.gif" alt="">
						</div>
					</div>
					<header class="article-header show-for-small-only">
						<h3><?php the_title(); ?></h3>
					</header>
				</article>
			</div>
		<?php endwhile; ?>
	</div><!-- .video-list -->
    <?php if (function_exists("pagination")) { pagination($videos->max_num_pages); } ?>
</div>