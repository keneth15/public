<?php
get_header(); ?>
	<?php 
		if (have_posts()) : while (have_posts()) : the_post(); 
		$sec_title = get_the_title();
				
		if( has_post_thumbnail() ) {
			$thumb = get_the_post_thumbnail_url(get_the_ID(),'full');
		} else {
			$thumb = '';
		}
	?>
	<main class="main main-<?php echo sanitize_title_with_dashes($sec_title); ?>" role="main" style="background-image: url('<?php echo $thumb; ?>');">
		<?php if( is_page('Home') ) : ?>
			<?php get_template_part( 'parts/page', 'home' ); ?>
		<?php elseif ( is_page('News') ) : ?>
			<?php get_template_part( 'parts/page', 'news' ); ?>
		<?php elseif ( is_page('Music') ) : ?>
			<?php get_template_part( 'parts/page', 'music' ); ?>
		<?php elseif ( is_page('Videos') ) : ?>
			<?php get_template_part( 'parts/page', 'videos' ); ?>
		<?php elseif ( is_page('Tour') ) : ?>
			<?php get_template_part( 'parts/page', 'tour' ); ?>
		<?php elseif ( is_page('Lyrics') ) : ?>
			<?php get_template_part( 'parts/page', 'lyrics' ); ?>
		<?php else :?>
			<div class="grid-container">
				<div class="grid-x">
					<div class="cell small-12 medium-10 large-10 medium-offset-1 large-offset-1">
						<?php get_template_part( 'parts/loop', 'page' ); ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	
	<?php endwhile; endif; ?>
<?php get_footer(); ?>