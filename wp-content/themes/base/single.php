<?php 
/**
 * The template for displaying all single posts and attachments
 */

get_header(); ?>

<main class="main cell" role="main">
	<div class="grid-container">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php if( is_singular( 'release' ) ) : ?>
				<?php get_template_part( 'parts/loop', 'release' ); ?>
			<?php elseif ( is_singular( 'video' ) ) : ?>
				<?php get_template_part( 'parts/loop', 'video' ); ?>
			<?php elseif ( is_singular( 'product' ) ) : ?>
				<?php get_template_part( 'parts/loop', 'product' ); ?>
			<?php else : ?>
				<div class="grid-x">
					<div class="cell small-12 medium-10 large-10 medium-offset-1 large-offset-1">
						<?php get_template_part( 'parts/loop', 'single' ); ?>
					</div>
				</div>
			<?php endif; ?>
		<?php endwhile; else : ?>
			<?php get_template_part( 'parts/content', 'missing' ); ?>
		<?php endif; ?>
	</div>
<?php get_footer(); ?>