<?php
function site_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way
        
    // Adding scripts file in the footer
    if( is_page('Home') ) {
      wp_enqueue_script( 'waypoint', get_template_directory_uri() . '/assets/scripts/jquery.waypoints.min.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts/js'), true );
    }

    wp_enqueue_script( 'slick', get_template_directory_uri() . '/assets/scripts/slick.min.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts/js'), true );
    wp_enqueue_script( 'fancybox', get_template_directory_uri() . '/assets/scripts/jquery.fancybox.min.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts/js'), true );

   // wp_enqueue_script( 'anime', get_template_directory_uri() . '/assets/scripts/anime.min.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts/js'), true );
    wp_enqueue_script( 'charming', get_template_directory_uri() . '/assets/scripts/charming.min.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts/js'), true );
   // wp_enqueue_script( 'wordfx', get_template_directory_uri() . '/assets/scripts/wordFx.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts/js'), true );



    wp_enqueue_script( 'site-js', get_template_directory_uri() . '/assets/scripts/scripts.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts/js'), true );
   
    // Register main stylesheet
    wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/styles/style.css', array(), filemtime(get_template_directory() . '/assets/styles/scss'), 'all' );

    //Style Ken
    wp_enqueue_style( 'twenty-twenty-one-style', get_template_directory_uri() . '/style.css', array(), wp_get_theme()->get( 'Version' ) );

    // Comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }
}
add_action('wp_enqueue_scripts', 'site_scripts', 999);