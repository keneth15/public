
// A $( document ).ready() block.
jQuery( document ).ready(function() {
  jQuery("#ae-cform-reg-item-email-1 input").attr("placeholder", "EMAIL ADDRESS");
  jQuery("#ae-cform-input-reg-mobilephone-1").attr("placeholder", "CELL PHONE #");
  jQuery("#ae-cform-reg-item-country-1 .ae-cform-select select[name=country] option:first").text("COUNTRY");

  jQuery(".text-email a").click(function(){
      jQuery(".ae-cform-modal-display-cta").click(); 
      return false;
  });
    //jQuery('#menu-item-80 a').addClass("hvr-sink");
      jQuery('.video-carousel').slick({
        asNavFor: '.play-video-carousel, .play-video-caption',
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        prevArrow:"<a class='a-left control-c prev slick-prev video-carousel-prev'></a>",
        nextArrow:"<a class='a-right control-c next slick-next video-carousel-next'></a>"
      });
      jQuery('.play-video-carousel').slick({
        asNavFor: '.video-carousel',
        dots: false,
        infinite: true,
        speed: 500,
        arrows: false,
        fade: true,
      });
      jQuery('.play-video-caption').slick({
        asNavFor: '.play-video-carousel',
        dots: false,
        infinite: true,
        speed: 500,
        arrows: false,
        fade: true,
      });



      jQuery('.release').slick({
        asNavFor: '.release-caption',
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        centerMode: true,
        prevArrow:"<a class='a-left slick-prev prev'></a>",
        nextArrow:"<a class='a-right slick-next next'></a>"
      });
      jQuery('.release-caption').slick({
        asNavFor: '.release',
        dots: false,
        infinite: true,
        speed: 500,
        arrows: false,
        fade: true,
      });

      jQuery('.product-carousel').slick({
        vertical: true,
        verticalSwiping: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        pauseOnHover: true,
        autoplay: false,
        dots: false,
        appendArrows: jQuery('.product-carousel-arrows'),
        prevArrow:"<a class='a-left slick-prev prev hide-for-small-only'></a>",
        nextArrow:"<a class='a-right slick-next next hide-for-small-only'></a>"
      });

      jQuery('.product-carousel-mobile').slick({
        asNavFor: '.product-caption-mobile',
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        pauseOnHover: true,
        autoplay: false,
        dots: false,
        appendArrows: jQuery('.product-carousel-arrows-mobile'),
        prevArrow:"<a class='a-left slick-prev prev'></a>",
        nextArrow:"<a class='a-right slick-next next'></a>"
      });
      jQuery('.product-caption-mobile').slick({
        asNavFor: '.product-carousel-mobile',
        dots: false,
        infinite: true,
        speed: 500,
        arrows: false,
        fade: true,
      });

      Fancybox.bind('[data-fancybox="video-gallery"]', {
        infinite: false
      });

      
      
});