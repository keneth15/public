<?php
/**
 * The template for displaying the footer. 
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */			
 ?>
 <?php if ( is_front_page() ) : ?>
 	<aside class="footer-side social-side show-for-small-only">
			<?php joints_social_nav(); ?>
 	</aside>
<?php endif; ?>
	<footer class="footer" role="contentinfo">
	<?php // Use shortcodes in form like Landing Page Template.
		echo do_shortcode( '[ae-custom-form id=1]' );
	?>
		<div class="grid-container">
			<div class="inner-footer grid-x">
				<div class="small-12 cell cell-center">
					<nav role="navigation">
						<?php joints_footer_links(); ?>
					</nav>
				</div>
			</div>
		</div>
	</footer><!-- end .footer -->
	<?php wp_footer(); ?>

</body>
</html>