<?php
/**
 * Template part for displaying a single post
 */

	$link = get_field('steam_download');
					
	if( $link ) {
		$url = $link['url'];
		$title = $link['title'];
		$target = $link['target'] ? $link['target'] : '_self';

		if($title) {
			$link_title = $link['title'];
		} else {
			$link_title = 'Stream / Download';
		}
	}

	$postID = get_the_ID();
	$releases = new WP_Query( array('post_type' => 'release', 'posts_per_page' => 4, 'orderby' => 'title', 'order' => 'ASC', 'post__not_in' => array($postID) ) );
?>
<div class="grid-container">
	<article id="post-<?php the_ID(); ?>" <?php post_class('featured'); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
		<div class="grid-x grid-padding-x align-middle">
			<div class="cell small-12 medium-6 large-5 medium-offset-1 large-offset-1">
				<div class="thumb">
					<?php the_post_thumbnail('full'); ?>
				</div>
			</div>
			<div class="cell small-12 medium-5 large-5">
				<div class="date"><?php the_time('m.d.y'); ?></div>
				<h3><?php the_title(); ?></h3>
				<?php if($link) : ?>
				<div class="cta">
					<a href="<?php echo $url; ?>" target="<?php echo $target; ?>" title="<?php echo $link_title; ?>" class="button"><?php echo $link_title; ?></a>
				</div>
				<?php endif; ?>
			</div>
			<div class="cell small-12 medium-10 large-10 medium-offset-1 large-offset-1">
				<div class="content">
					<?php the_content(); ?>
				</div>
			</div>
		</div>

		<footer class="article-footer">
			<?php wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'jointswp' ), 'after'  => '</div>' ) ); ?>
		</footer>
	</article>
</div>
<?php wp_reset_query(); ?>

<?php if( $releases->have_posts() ) : ?>
<section class="latest">
	<div class="grid-container">

	<h2>Latest Releases</h2>
		<div class="music-list grid-x grid-padding-x">
			<?php 
				while ( $releases->have_posts() ) : $releases->the_post(); 
				$link = get_field('steam_download');
							
				if( $link ) {
					$url = $link['url'];
					$title = $link['title'];
					$target = $link['target'] ? $link['target'] : '_self';
			
					if($title) {
						$link_title = $link['title'];
					} else {
						$link_title = 'Stream / Download';
					}
				}
			?>
				<div class="cell medium-4">
					<article id="post-<?php the_ID(); ?>" <?php post_class('article'); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
						<header class="article-header">
							<div class="date"><?php the_time('m.d.y'); ?></div>
							<h3><?php the_title(); ?></h3>
							<?php if($link) : ?>
							<div class="cta show-for-small-only">
								<a href="<?php echo $url; ?>" target="<?php echo $target; ?>" title="<?php echo $link_title; ?>" class="button"><?php echo $link_title; ?></a>
							</div>
							<?php endif; ?>
						</header>
						<div class="article-thumb">
							<div class="article-thumb_overlay hide-for-small-only">
								<?php if($link) : ?>
								<div class="cta">
									<a href="<?php echo $url; ?>" target="<?php echo $target; ?>" title="<?php echo $link_title; ?>" class="button"><?php echo $link_title; ?></a>
								</div>
								<?php endif; ?>
							</div>

							<?php the_post_thumbnail('full'); ?>
						</div>

					</article>
				</div>
			<?php endwhile; ?>
		</div><!-- .music-list -->
	</div>
</section>
<?php endif; ?>