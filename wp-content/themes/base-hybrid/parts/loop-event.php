<?php
/**
 * Template part for displaying a single post
 */
$postID = get_the_ID();
$videos = new WP_Query( array('post_type' => 'video', 'posts_per_page' => 4, 'orderby' => 'title', 'order' => 'ASC', 'post__not_in' => array($postID) ) );
$yt_id = get_field('youtube_id');
?>
<div class="grid-container">
	<article id="post-<?php the_ID(); ?>" <?php post_class('featured'); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
		<div class="grid-x grid-padding-x align-middle">
			<div class="cell small-12">
				<div class="thumb">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $yt_id; ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
			<div class="cell small-12">
				<div class="date"><?php the_time('m.d.y'); ?></div>
				<h3><?php the_title(); ?></h3>
				<div class="content">
					<?php the_content(); ?>
				</div>
			</div>
		</div>

		<footer class="article-footer">
			<?php wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'jointswp' ), 'after'  => '</div>' ) ); ?>
		</footer>
	</article>
</div>
<?php wp_reset_query(); ?>

<?php if( $videos->have_posts() ) : ?>
<section class="latest">
	<div class="grid-container">

	<h2>Latest Videos</h2>
		<div class="video-list grid-x grid-padding-x">
		<?php 
			while ( $videos->have_posts() ) : $videos->the_post(); 
			$yt_id = get_field('youtube_id');
		
			if(wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) )) {
				$thumb = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
			} else {
				$thumb = 'https://img.youtube.com/vi/'.$yt_id.'/maxresdefault.jpg';
			}
		?>
			<div class="cell medium-4">
				<article id="post-<?php the_ID(); ?>" <?php post_class('article'); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
					<div class="article-thumb">
						<div class="article-thumb_overlay">
							<h3><?php the_title(); ?></h3>
							<a href="<?php the_permalink(); ?>" title="Watch <?php the_title(); ?>" class="button secondary">Play</a>
						</div>
				
						<div class="article-thumb" style="background-image: url('<?php echo $thumb; ?>');">
							<img src="<?php echo $GLOBALS['dir']; ?>/images/blank-video.gif" alt="">
						</div>
					</div>

				</article>
			</div>
		<?php endwhile; ?>
		</div><!-- .video-list -->
	</div>
</section>
<?php endif; ?>