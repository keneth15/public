<?php
/**
 * Template part for displaying page content in page.php
 */
	if( has_post_thumbnail() ) {
		$thumb = get_the_post_thumbnail_url(get_the_ID(), 'full');
	} else {
		// $thumb = $GLOBALS['dir'].'/images/blank-news.png';
		$thumb = '';
		$noFeatured = 'no-featured';
	}
?>


<article id="post-<?php the_ID(); ?>" <?php post_class($noFeatured); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
	
	<header class="article-header">
		<?php if( has_post_thumbnail() ) : ?>
			<?php the_post_thumbnail('full'); ?>
		<?php endif; ?>
		<?php if( !is_page('About') ) : ?>
		<h3><?php the_title(); ?></h3>
		<?php endif; ?>

	</header>
    <div class="article-content" itemprop="text">
	    <?php the_content(); ?>
	</div>
	<footer class="article-footer">
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'jointswp' ), 'after'  => '</div>' ) ); ?>
	</footer>
</article>