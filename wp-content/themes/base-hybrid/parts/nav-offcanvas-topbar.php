<?php
/**
 * The off-canvas menu uses the Off-Canvas Component
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
?>

<div class="top-bar" id="top-bar-menu">
	<button class="show-for-small-only menu-bars" data-toggle="off-canvas"><i class="fas fa-bars"></i></button>
	<?php if ( has_custom_logo() ) : ?>
		<h1 class="logo"><?php the_custom_logo(); ?></h1>
	<?php else : ?>
		<h1 class="logo text"><a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a></h1>
	<?php endif; ?>
</div>

<div class="header-side nav-side hide-for-small-only">
	<?php joints_top_nav(); ?>	
</div>

<aside class="header-side social-side hide-for-small-only">
	<?php joints_social_nav(); ?>
</aside>