<?php
/**
 * Template part for displaying posts
 *
 * Used for single, index, archive, search.
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('article'); ?> role="article">			
	<header class="article-header">
		<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
		<?php // get_template_part( 'parts/content', 'byline' ); ?>
	</header>
					
	<div class="article-content" itemprop="text">
		<!-- <a href="<?php// the_permalink() ?>"><?php// the_post_thumbnail('full'); ?></a> -->
		<?php the_excerpt(); ?>
		<?php // the_content('<button class="tiny">' . __( 'Read more...', 'jointswp' ) . '</button>'); ?>
	</div>
				    						
</article>
