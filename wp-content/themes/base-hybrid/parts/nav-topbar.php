<?php
/**
 * The off-canvas menu uses the Off-Canvas Component
 *
 * For more info: http://jointswp.com/docs/responsive-navigation/
 */
?>


	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell small-3 medium-12">
				<?php joints_top_nav(); ?>
			</div>
			<div class="cell small-9 hide-desktop padding-top-md">
				<div class="home-url-mobile">
					<a href="<?php echo home_url(); ?>" class="font-white mega-title-web-mobile-outside">August 08</a>
				</div>
			</div>
		</div>
	</div>
