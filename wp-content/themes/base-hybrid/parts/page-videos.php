<?php
	/**
	 * Template part for displaying home content
	 */
	$videos = new WP_Query( array('post_type' => 'video', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'DESC' ) );
?>

	<?php if( $videos->have_posts() ) : ?>
		<div class="grid-container grid-x">
			<div class="cell small-12 medium-12">
				<div class="parent position-relative">
						<div class="tv-bk-video vertical-align-content position-relative">
							<img class="position-absolute tv-video" src="<?php echo IMAGES ?>/videos-tv-bk-new.png" alt="disc-bk">
							<div class="video-carousel masked">
								<?php while ( $videos->have_posts() ) : $videos->the_post(); ?>
								<?php $video_id = get_field( 'youtube_id', $post->ID ); ?>
										<div>
											<img class="thumb-video" src="https://img.youtube.com/vi/<?php echo $video_id; ?>/sddefault.jpg" alt="">
										</div>
								<?php endwhile; ?>
							</div>
						</div>
						<div class="position-relative">
							<div class="videos play-video-carousel">
								<?php while ( $videos->have_posts() ) : $videos->the_post(); ?>
								<?php $video_id = get_field( 'youtube_id', $post->ID ); ?>
										<div class="article-video type-video position-relative">
											<div class="center">
												<a class="button-video" data-fancybox="video-gallery" href="https://www.youtube.com/watch?v=<?php echo $video_id; ?>"></a>
												<!--<a class="button-video" data-id="<?php echo $video_id; ?>" href="#"></a>-->
												<!--<a class="button-video-stop" data-id="<?php echo $video_id; ?>" href="#"></a>-->
											</div>
										</div>
								<?php endwhile; ?>
							</div>
						</div>
				</div>
			</div>
		</div>
	<?php endif; wp_reset_query(); ?>
</main>
<main class="class-bottom">
	<div class="play-video-caption">
		<?php while ( $videos->have_posts() ) : $videos->the_post(); ?>
		<?php $steam_download = get_field( 'steam_download', $post->ID ); ?>
		<?php //print_r($steam_download['url']); ?>
				<div class="video-description">
					<h1 class="video-title"><?php the_title(); ?></h1>
				</div>
		<?php endwhile; ?>
	</div>
</main>