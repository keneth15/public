<?php
	/**
	 * Template part for displaying home content
	 */
	$videos = new WP_Query( array('post_type' => 'video', 'posts_per_page' => 1, 'orderby' => 'date', 'order' => 'DESC' ) );
?>

<?php if( $videos->have_posts() ) : ?>

	<div class="grid-container grid-x">
		<div class="cell small-12 medium-12">
			<div class="parent-home position-relative">
				<div class="tv-bk position-relative">
					<?php while ( $videos->have_posts() ) : $videos->the_post(); ?>
						<?php $video_id = get_field( 'youtube_id', $post->ID ); ?>
						<img class="position-absolute tv1" src="<?php echo IMAGES ?>/tv-hand-crop.png" alt="disc-bk">
						<img class="position-absolute thumb-video1" src="https://img.youtube.com/vi/<?php echo $video_id; ?>/sddefault.jpg" alt="">
						<a class="button-video" data-fancybox="video-gallery" href="https://www.youtube.com/watch?v=<?php echo $video_id; ?>">
							<img class="position-absolute button1" src="<?php echo IMAGES ?>/play-button.png" alt="">
						</a>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; wp_reset_query(); ?>
</main>
