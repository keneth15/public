<?php 
	$merch = new WP_Query( array('post_type' => 'product', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'DESC' ) ); 
	//print_r($merch);
	/*while ($feat->have_posts()) { $feat->the_post(); 
		$featID = get_the_ID();
	}
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 
	$release = new WP_Query( array('post_type' => 'release', 'posts_per_page' => 9, 'orderby' => 'date', 'order' => 'DESC', 'paged' => $paged, 'post__not_in' => array($featID) ) ); */
?>
	<div class="grid-container grid-x hide-for-small-only">
		<div class="cell small-12 medium-10 medium-offset-1">
			<div class="featured">
				<div class="product-carousel">
					<?php
					/*for($count=0; $merch->have_posts(); $count++) : 
						$merch->the_post();
						$open = !($count%3) ? '<div class="div-no-cierra"><div class="grid-x">' : ''; //Create open wrapper if count is divisible by 3
						$close = !($count%3) && $count ? '</div></div>' : ''; //Close the previous wrapper if count is divisible by 3 and greater than 0
						echo $close.$open;*/
					?>
					<?php
						while ( $merch->have_posts() ) : $merch->the_post(); 
						if ($c % 3 == 0) :
							if($c>0){
								echo '</div></div>';
							}else{
								echo '';
							}
							//echo $c > 0 ? '</div></div>' : '';
							echo '<div class="div-no-cierra"><div class="grid-x">';
						endif;
					?>
					<?php $link_product = get_field( 'link', $post->ID ); ?>
							<div class="cell medium-4">
								<?php the_post_thumbnail('full', array('class'=>'product')); ?>
								<div class="padding-top-lg padding-bottom-lg">
									<h1 class="product-title font-white"><?php the_title(); ?></h1>
									<div class="center padding-top-md"><a class="button-product font-white" target="_blank" href="<?php echo $link_product['url']; ?>"><?php echo $link_product['title']; ?></a></div>
								</div>
							</div>
					<?php 
						$c++;
						endwhile; 
					?>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="grid-container grid-x show-for-small-only">
		<div class="cell small-10 small-offset-1">
			<div class="featured-mobile">
				<div class="product-carousel-mobile">
					<?php while ( $merch->have_posts() ) : $merch->the_post(); ?>
						<?php $link_product = get_field( 'link', $post->ID ); ?>
						<div class="image-product">
							<?php the_post_thumbnail('full', array('class'=>'product')); ?>
						</div>
					<?php endwhile; ?>
				</div>
			</div>	
		</div>
	</div>
</main>
<main class="class-bottom">
		<div class="product-carousel-arrows position-relative hide-for-small-only"></div>
			<div class="product-caption-mobile show-for-small-only">
				<?php while ( $merch->have_posts() ) : $merch->the_post(); ?>
				<?php $link_product = get_field( 'link', $post->ID ); ?>
				<div class="merch-description">
					<h1 class="product-title font-white"><?php the_title(); ?></h1>
					<div class="center"><a class="button-product font-white" target="_blank" href="<?php echo $link_product['url']; ?>"><?php echo $link_product['title']; ?></a></div>
				</div>	
				<?php endwhile; ?>
			</div>
			<div class="product-carousel-arrows-mobile paddin-top-md show-for-small-only"></div>
		</div>
</main>
<style>
	/*
.product-carousel-arrows .slick-prev{
	top:auto !important;
	bottom:10%;
    left: 47%;
     top: -25px; 
	transform: rotate(90deg) translate(-50%, -50%) !important;
}
.product-carousel-arrows .slick-next {
	top:auto !important;
	bottom:10%;
    right: 47%;
    top: -25px; 
	transform: rotate(90deg) translate(-50%, -50%) !important;
}
.product-carousel-arrows{
	height:100%;
}*/
</style>