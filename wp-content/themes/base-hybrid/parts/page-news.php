<?php
	/**
	 * Template part for displaying home content
	 */
	$videos = new WP_Query( array('post_type' => 'video', 'posts_per_page' => 1, 'orderby' => 'date', 'order' => 'DESC' ) );
?>

<?php if( $videos->have_posts() ) : ?>

	<div class="grid-container grid-x">
		<div class="cell small-12 medium-12" style="height:100%">
			<div class="parent-home position-relative" style="height:100%">
				<div class="tv-bk position-relative" style="height:100%">
				<table >
					<tbody style="background-color:transparent; border:0;">
						<tr>
					 		<td style="vertical-align: middle;">
							 	<div class="home-carousel masked" style="background: url('<?php echo IMAGES ?>/tv-hand-crop.png') center center no-repeat; height:100%; width:100%; z-index:1;">
									<?php while ( $videos->have_posts() ) : $videos->the_post(); ?>
									<?php $video_id = get_field( 'youtube_id', $post->ID ); ?>
											<div>
												<img class="thumb-video" src="https://img.youtube.com/vi/<?php echo $video_id; ?>/sddefault.jpg" alt="" style="max-width: 50% !important;">
											</div>
									<?php endwhile; ?>
								</div>
								<div class="home play-video" style="align-self: flex-end;">
									<?php while ( $videos->have_posts() ) : $videos->the_post(); ?>
									<?php $video_id = get_field( 'youtube_id', $post->ID ); ?>
											<div class="article-video type-video position-relative">
												<a class="button-video" data-fancybox="video-gallery" href="https://www.youtube.com/watch?v=<?php echo $video_id; ?>"></a>
											</div>
									<?php endwhile; ?>
								</div>
					 		</td>
				 		</tr>
					</tbody>
				</table>
					<!--<img class="position-absolute tv" src="<?php echo IMAGES ?>/tv-hand-crop.png" alt="disc-bk" style="align-self: flex-end;">
					<div class="home-carousel masked">
						<?php while ( $videos->have_posts() ) : $videos->the_post(); ?>
						<?php $video_id = get_field( 'youtube_id', $post->ID ); ?>
								<div>
									<img class="thumb-video" src="https://img.youtube.com/vi/<?php echo $video_id; ?>/sddefault.jpg" alt="">
								</div>
						<?php endwhile; ?>
					</div>
					<div class="home play-video" style="align-self: flex-end;">
						<?php while ( $videos->have_posts() ) : $videos->the_post(); ?>
						<?php $video_id = get_field( 'youtube_id', $post->ID ); ?>
								<div class="article-video type-video position-relative">
									<a class="button-video" data-fancybox="video-gallery" href="https://www.youtube.com/watch?v=<?php echo $video_id; ?>"></a>
								</div>
						<?php endwhile; ?>
					</div>-->
				</div>
			</div>
		</div>
	</div>

<?php endif; wp_reset_query(); ?>
</main>


<?php
	/**
	 * Template part for displaying home content
	 */
	$videos = new WP_Query( array('post_type' => 'video', 'posts_per_page' => 1, 'orderby' => 'date', 'order' => 'DESC' ) );
?>

<?php if( $videos->have_posts() ) : ?>

	<div class="grid-container grid-x">
		<div class="cell small-12 medium-12">
			<div class="parent-home position-relative">
				<div class="tv-bk position-relative">
					<img class="position-absolute tv" src="<?php echo IMAGES ?>/tv-hand-crop.png" alt="disc-bk">
					<div class="home-carousel masked">
						<?php while ( $videos->have_posts() ) : $videos->the_post(); ?>
						<?php $video_id = get_field( 'youtube_id', $post->ID ); ?>
								<div>
									<img class="thumb-video" src="https://img.youtube.com/vi/<?php echo $video_id; ?>/sddefault.jpg" alt="">
								</div>
						<?php endwhile; ?>
					</div>
					<div class="home play-video">
						<?php while ( $videos->have_posts() ) : $videos->the_post(); ?>
						<?php $video_id = get_field( 'youtube_id', $post->ID ); ?>
								<div class="article-video type-video position-relative">
									<a class="button-video" data-fancybox="video-gallery" href="https://www.youtube.com/watch?v=<?php echo $video_id; ?>"></a>
									<!--<a dara-fancybox class="button-video home-video-fancybox" href="https://www.youtube.com/watch?v=<?php echo $video_id; ?>"></a>-->
										<!--<a class="button-video" data-id="<?php echo $video_id; ?>" href="#"></a>-->
										<!--<a class="button-video-stop" data-id="<?php echo $video_id; ?>" href="#"></a>-->
								</div>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php endif; wp_reset_query(); ?>
</main>