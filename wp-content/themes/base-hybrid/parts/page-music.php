<?php 
	$release = new WP_Query( array('post_type' => 'release', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'DESC' ) ); 
	/*while ($feat->have_posts()) { $feat->the_post(); 
		$featID = get_the_ID();
	}
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 
	$release = new WP_Query( array('post_type' => 'release', 'posts_per_page' => 9, 'orderby' => 'date', 'order' => 'DESC', 'paged' => $paged, 'post__not_in' => array($featID) ) ); */
?>
	
		<div class="grid-container grid-x">
			<div class="cell small-12 medium-10 medium-offset-1">
				<div class="space-music space-music-mobile"></div>
				<div class="featured position-relative">
					<img class="position-absolute disc-image" src="<?php echo IMAGES ?>/disc-new.png" alt="disc-bk">
					<div class="release">
						<?php while ( $release->have_posts() ) : $release->the_post(); ?>
								<div>
									<?php the_post_thumbnail('full', array('class'=>'disc')); ?>
								</div>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
		</div>
	
</main>
<!--<main class="class-middle"></main>-->
<main class="class-bottom">
	<div class="release-caption">
		<?php while ( $release->have_posts() ) : $release->the_post(); ?>
		<?php $steam_download = get_field( 'steam_download', $post->ID ); ?>
		<?php //print_r($steam_download['url']); ?>
				<div class="music-description">
					<h1 class="release-title"><?php the_title(); ?></h1>
					<div class="center download">
						<a class="button-release" target="_blank" href="<?php echo $steam_download['url']; ?>"><?php echo $steam_download['title']; ?></a>
					</div>
				</div>
		<?php endwhile; ?>
	</div>
</main>
