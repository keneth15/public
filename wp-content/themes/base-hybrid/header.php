<?php
/**
 * The template for displaying the header
 *
 * This is the template that displays all of the <head> section
 *
 */
?>

<!doctype html>

  <html class="no-js"  <?php language_attributes(); ?>>

	<head>
		<meta charset="utf-8">
		
		<!-- Force IE to use the latest rendering engine available -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta class="foundation-mq">
		
		<!-- If Site Icon isn't set in customizer -->
		<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
			<!-- Icons & Favicons -->
			<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
			<link href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-icon-touch.png" rel="apple-touch-icon" />	
	    <?php } ?>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<!--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css"/>-->

		<script src="https://kit.fontawesome.com/232cdc2521.js" crossorigin="anonymous"></script>
		
		<?php wp_head(); ?>

	</head>
			
	<body <?php body_class(); ?>>
		<header class="main-header" role="banner">
			<?php get_template_part( 'parts/nav', 'topbar' ); ?>
		</header> <!-- end .header -->
	<?php if ( is_front_page() ) : ?>
		<main class="class-home">
	<?php else: ?>	
		<main class="class-internal class-section-<?php echo $post->post_name; ?> ">
	<?php endif; ?>
		<aside class="header-side social-side hide-for-small-only">
			<?php joints_social_nav(); ?>
		</aside>