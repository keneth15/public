<?php
/**
 * The template for displaying 404 (page not found) pages.
 *
 * For more info: https://codex.wordpress.org/Creating_an_Error_404_Page
 */

get_header(); ?>
	
	<div class="small-12 medium-12 large-12 cell main-404">
		<div class="grid-container">
			<div class="grid-x grid-padding-x error404">
				<div class="cell small-12 medium-6 medium-offset-3">
					<article class="article content-not-found">
						<header class="article-header">
							<h2><?php _e( '404 - Article Not Found', 'jointswp' ); ?></h2>
						</header>

						<div class="article-content">
							<p><?php _e( 'The article you were looking for was not found, but maybe try looking again!', 'jointswp' ); ?></p>
						</div> 

						<div class="search">
							<?php get_search_form(); ?>
						</div>
					</article>
				</div>
			</div>
		</div>
</main>
<main class="class-bottom min-height"></main>
<?php get_footer(); ?>