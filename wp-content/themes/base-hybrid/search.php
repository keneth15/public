<?php 
/**
 * The template for displaying search results pages
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 */
 	
get_header(); ?>


	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell small-12 medium-8 medium-offset-2">
				<header>
					<h1 class="archive-title"><?php _e( 'Search Results for:', 'jointswp' ); ?> <?php echo esc_attr(get_search_query()); ?></h1>
				</header>

				<div class="list">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php get_template_part( 'parts/loop', 'archive' ); ?>
					<?php endwhile; ?>
						<?php joints_page_navi(); ?>
					<?php else : ?>
						<?php get_template_part( 'parts/content', 'missing' ); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	</main>
<main class="class-bottom min-height"></main>
<?php get_footer(); ?>
