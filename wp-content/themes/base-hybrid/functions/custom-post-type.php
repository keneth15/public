<?php
/* joints Custom Post Type Example
This page walks you through creating 
a custom post type and taxonomies. You
can edit this one or copy the following code 
to create another one. 

I put this in a separate file so as to 
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/


function cp_release() { 
	// creating (registering) the custom type 
	register_post_type( 'release', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Releases', 'jointswp'), /* This is the Title of the Group */
			'singular_name' => __('Release', 'jointswp'), /* This is the individual type */
			'all_items' => __('All Releases', 'jointswp'), /* the all items menu item */
			'add_new' => __('Add New', 'jointswp'), /* The add new menu item */
			'add_new_item' => __('Add New Release', 'jointswp'), /* Add New Display Title */
			'edit' => __( 'Edit', 'jointswp' ), /* Edit Dialog */
			'edit_item' => __('Edit Release', 'jointswp'), /* Edit Display Title */
			'new_item' => __('New Release', 'jointswp'), /* New Display Title */
			'view_item' => __('View Release', 'jointswp'), /* View Display Title */
			'search_items' => __('Search Release', 'jointswp'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'jointswp'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'jointswp'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is Release post type', 'jointswp' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 7, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-album', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'release', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'release', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'author', 'thumbnail', 'sticky')
	 	) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	// register_taxonomy_for_object_type('category', 'release');
	/* this adds your post tags to your custom post type */
	// register_taxonomy_for_object_type('post_tag', 'release');
	
} function cp_video() { 
	register_post_type( 'video',
		array('labels' => array(
			'name' => __('Videos', 'jointswp'),
			'singular_name' => __('Video', 'jointswp'),
			'all_items' => __('All Videos', 'jointswp'),
			'add_new' => __('Add New', 'jointswp'),
			'add_new_item' => __('Add New Video', 'jointswp'),
			'edit' => __( 'Edit', 'jointswp' ),
			'edit_item' => __('Edit Videos', 'jointswp'),
			'new_item' => __('New Video', 'jointswp'),
			'view_item' => __('View Video', 'jointswp'),
			'search_items' => __('Search Video', 'jointswp'),
			'not_found' =>  __('Nothing found in the Database.', 'jointswp'),
			'not_found_in_trash' => __('Nothing found in Trash', 'jointswp'),
			'parent_item_colon' => ''
			),
			'description' => __( 'This is Video post type', 'jointswp' ),
			'public' => true,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8,
			'menu_icon' => 'dashicons-video-alt2', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'video', 'with_front' => false ),
			'has_archive' => 'video',
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	)
	);
	// register_taxonomy_for_object_type('category', 'video');
	/* this adds your post tags to your custom post type */
	// register_taxonomy_for_object_type('post_tag', 'video');
}
function cp_product() { 
	register_post_type( 'product',
		array('labels' => array(
			'name' => __('Products', 'jointswp'),
			'singular_name' => __('Product', 'jointswp'),
			'all_items' => __('All Products', 'jointswp'),
			'add_new' => __('Add New', 'jointswp'),
			'add_new_item' => __('Add New Product', 'jointswp'),
			'edit' => __( 'Edit', 'jointswp' ),
			'edit_item' => __('Edit Products', 'jointswp'),
			'new_item' => __('New Product', 'jointswp'),
			'view_item' => __('View Product', 'jointswp'),
			'search_items' => __('Search Product', 'jointswp'),
			'not_found' =>  __('Nothing found in the Database.', 'jointswp'),
			'not_found_in_trash' => __('Nothing found in Trash', 'jointswp'),
			'parent_item_colon' => ''
			),
			'description' => __( 'This is Product post type', 'jointswp' ),
			'public' => true,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 9,
			'menu_icon' => 'dashicons-cart', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'product', 'with_front' => false ),
			'has_archive' => 'product',
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	)
	);
	// register_taxonomy_for_object_type('category', 'product');
	/* this adds your post tags to your custom post type */
	// register_taxonomy_for_object_type('post_tag', 'product');
}
function cp_lyric() { 
	register_post_type( 'lyric',
		array('labels' => array(
			'name' => __('Lyrics', 'jointswp'),
			'singular_name' => __('Lyric', 'jointswp'),
			'all_items' => __('All Lyrics', 'jointswp'),
			'add_new' => __('Add New', 'jointswp'),
			'add_new_item' => __('Add New Lyric', 'jointswp'),
			'edit' => __( 'Edit', 'jointswp' ),
			'edit_item' => __('Edit Lyrics', 'jointswp'),
			'new_item' => __('New Lyric', 'jointswp'),
			'view_item' => __('View Lyric', 'jointswp'),
			'search_items' => __('Search Lyric', 'jointswp'),
			'not_found' =>  __('Nothing found in the Database.', 'jointswp'),
			'not_found_in_trash' => __('Nothing found in Trash', 'jointswp'),
			'parent_item_colon' => ''
			),
			'description' => __( 'This is Lyric post type', 'jointswp' ),
			'public' => true,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8,
			'menu_icon' => 'dashicons-playlist-audio', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'lyric', 'with_front' => false ),
			'has_archive' => 'lyric',
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'sticky')
	 	)
	);
	// register_taxonomy_for_object_type('category', 'lyric');
	/* this adds your post tags to your custom post type */
	// register_taxonomy_for_object_type('post_tag', 'lyric');
}

	// enable/disable custom post types
	add_action( 'init', 'cp_release');
	add_action( 'init', 'cp_video');
	add_action( 'init', 'cp_product');
	// add_action( 'init', 'cp_lyric');
	
	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
	
	// now let's add custom categories (these act like categories)
    register_taxonomy( 'custom_cat', 
    	array('custom_type'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    	array('hierarchical' => true,     /* if this is true, it acts like categories */             
    		'labels' => array(
    			'name' => __( 'Custom Categories', 'jointswp' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'Custom Category', 'jointswp' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Custom Categories', 'jointswp' ), /* search title for taxomony */
    			'all_items' => __( 'All Custom Categories', 'jointswp' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Custom Category', 'jointswp' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Custom Category:', 'jointswp' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Custom Category', 'jointswp' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update Custom Category', 'jointswp' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Custom Category', 'jointswp' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Custom Category Name', 'jointswp' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true, 
    		'show_ui' => true,
    		'query_var' => true,
    		'rewrite' => array( 'slug' => 'custom-slug' ),
    	)
    );   
    
	// now let's add custom tags (these act like categories)
    register_taxonomy( 'custom_tag', 
    	array('custom_type'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    	array('hierarchical' => false,    /* if this is false, it acts like tags */                
    		'labels' => array(
    			'name' => __( 'Custom Tags', 'jointswp' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'Custom Tag', 'jointswp' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Custom Tags', 'jointswp' ), /* search title for taxomony */
    			'all_items' => __( 'All Custom Tags', 'jointswp' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Custom Tag', 'jointswp' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Custom Tag:', 'jointswp' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Custom Tag', 'jointswp' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update Custom Tag', 'jointswp' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Custom Tag', 'jointswp' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Custom Tag Name', 'jointswp' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true,
    		'show_ui' => true,
    		'query_var' => true,
    	)
    ); 
    
    /*
    	looking for custom meta boxes?
    	check out this fantastic tool:
    	https://github.com/CMB2/CMB2
    */
