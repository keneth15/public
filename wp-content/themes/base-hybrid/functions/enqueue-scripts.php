<?php
function site_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way
        

  //wp_register_script( 'jquery-version', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js', array('jquery'),null,true);  
  //wp_enqueue_script('jquery-version');

  wp_register_script( 'foundation-js', get_template_directory_uri(). '/assets/scripts/vendor/foundation.min.js', array('jquery'),null,true);
  wp_enqueue_script('foundation-js');

  wp_register_script( 'app-foundation-js', get_template_directory_uri(). '/assets/scripts/app.js', array('jquery'),null,true);
  wp_enqueue_script('app-foundation-js');

  wp_register_script( 'slick-js', get_template_directory_uri(). '/assets/slick/slick.min.js', array('jquery'),null,true);
  wp_enqueue_script('slick-js');

  wp_register_script( 'fancybox-js', get_template_directory_uri(). '/assets/fancybox/fancybox.umd.js', array('jquery'),null,true);
  wp_enqueue_script('fancybox-js');

  wp_register_script( 'myscripts-js', get_template_directory_uri(). '/assets/scripts/my-scripts.js', array('jquery'),null,true);
  wp_enqueue_script('myscripts-js');

  wp_register_style( 'foundation-css', get_template_directory_uri().'/assets/styles/foundation.min.css',array(), null  );   
  wp_enqueue_style( 'foundation-css' );

  wp_register_style( 'slick-css', get_template_directory_uri().'/assets/slick/slick.css',array(), null  );   
  wp_enqueue_style( 'slick-css' );

  wp_register_style( 'slick-theme-css', get_template_directory_uri().'/assets/slick/slick-theme.css',array(), null );   
  wp_enqueue_style( 'slick-theme-css' );

  wp_register_style( 'fancybox-css', get_template_directory_uri(). '/assets/fancybox/fancybox.css', array(),null);
  wp_enqueue_style('fancybox-css');

  wp_register_style( 'hover-css', get_template_directory_uri(). '/assets/styles/hover-min.css', array(),null);
  wp_enqueue_style('hover-css');

  wp_register_style( 'styles', get_stylesheet_uri(),array(), '2.0');   
  wp_enqueue_style( 'styles' );

    // Comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }
}
add_action('wp_enqueue_scripts', 'site_scripts', 999);