<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '24fXO4E7kOfD0Wu/JW8VGhRc6hKGvT8CUptSSu2pLzijvBBtxtsEFk7RQ9bBG831bUbednsTMRwFDFEpFb1/wA==');
define('SECURE_AUTH_KEY',  'Sx8dP6B3li35b08QVIQAEaP5dE4V9IRlHArlEQpQKpL0TQ9tzw5PMM9rEjoAKzEtRZcqthPj0GQo+SvQqz++jg==');
define('LOGGED_IN_KEY',    'vgN3c7i5yINw0kFmqJDcFjXlUffFUW24/yekvOHriJ0THz7YXKRLPkpsd5kZn48HE6ufQ4FB95+MWs6TLZraRg==');
define('NONCE_KEY',        'HRmOur8iA1z0gmhOf7/CMM3qwzwrpz6BhQKGVZ7f0ZMAmz+IFleYFMVjkTJ6oo4/I8AXmNmgAaa3WYuKVp/PTg==');
define('AUTH_SALT',        'CEAr4w6yFiZdkmSIxfxJSD9R/eB9e8NR6GeFIGXI02ol+0QiqQT7oYRZwMe03e+XITsV+09RoWa4/qPtb9O1rw==');
define('SECURE_AUTH_SALT', 'U4UO2Uzqf+wiVKltk9LkLKKtAYbdB0S+BVLQyOx2zzD/kzYK7GotVbcqXBk1WWzVhQYY7o04oukwAy6nsEHeQg==');
define('LOGGED_IN_SALT',   '8HfURSpn3unv5oPpCa8PqnhWCsEa367nfGCj7p+XnrayyBo1XmmBExBBl5OSRJYUVgTZV1UbcvQHoZuXv0dVcg==');
define('NONCE_SALT',       'PQAXV1h8H95NuB88kCSrQPNz4VP62dsOwIx0jf8O1jJ72Y6lXgwPHlubSal9C4AJsSWkTJFKxCJdp4ouyWHiKw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';
 
 

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
